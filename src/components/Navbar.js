import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import brandLogo from "../resources/Aztlan-logo-23.png";
import Link from "next/link";
import muiStyles from "../styles/navbarStyles";
import { useTheme } from "@material-ui/core";
import useMediaQuery from "@material-ui/core/useMediaQuery";

const useStyles = makeStyles(muiStyles);

export default function Navbar() {
  const theme = useTheme();
  const classes = useStyles();
  const mobile = useMediaQuery(theme.breakpoints.down("xs"));

  return (
    <section
      // className={classes.section}
      className="navbar"
      style={{ height: "20vh" }}
    >
      <Grid
        container
        justify="space-between"
        alignItems="center"
        className="gridposition"
        // className={classes.gridposition}
      >
        <Grid item sm={5}>
          <img
            src={brandLogo}
            // className={classes.logo}
            className="logo"
            alt="brand logo"
          />
        </Grid>
        <Grid item sm={7}>
          <ul
            // className={classes.list}
            className="list"
          >
            <li>
              <Link href="#about">
                <a>ABOUT</a>
              </Link>
            </li>
            <li>
              <Link href="#products">
                <a>PRODUCTS</a>
              </Link>
            </li>
            <li>
              <Link href="#contact">
                <a>CONTACT US</a>
              </Link>
            </li>
          </ul>
        </Grid>
      </Grid>
    </section>
  );
}
