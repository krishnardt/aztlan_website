import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import {
  Button,
  Link,
  Typography,
  TextField,
  useMediaQuery,
  useTheme,
  Box,
} from "@material-ui/core";
import muiStyles from "../styles/homeviewStyles";
import workpeerImg from "../resources/image_final.png";
import logoWhite from "../resources/Aztlan-logo-23-white.png";

// const useStyles = makeStyles(muiStyles);

const HomeView = () => {
  // const classes = useStyles();
  const theme = useTheme();
  const mobile = useMediaQuery(theme.breakpoints.down("xs"));

  return (
    <div>
      <section
        // className={classes.section1}
        className="section1"
      >
        <Grid item>
          <Typography
            variant="h1"
            color="textPrimary"
            gutterBottom
            // className={classes.homehead}
          >
            <Box className="homehead">
              Creating products that sell themselves.
            </Box>
          </Typography>
        </Grid>
        <Grid item md={7}>
          <Typography
            gutterbottom
            // className={classes.homeTagLine}
          >
            <Box className="homeTagLine">
              We create products that are rooted in sound engineering principles
              and make a difference in the way you interact with the world.
            </Box>
          </Typography>
        </Grid>
        <Button
          variant="contained"
          color="primary"
          // className={classes.contactButton}
          className="contactButton"
        >
          Contact Us
        </Button>
        <Grid
          item
          // className={classes.gridposition}
        >
          <Box className="gridposition">
            <ul
            // className={classes.list}
            >
              <li>
                <a href="#products">products</a>
                <div>real products that are making a difference</div>
              </li>
              <li>
                <a href="#about">about us</a>
                <div> who we are and what we do best</div>
              </li>
              <li>
                <a href="#contact">contact</a>
                <div>
                  get in touch for queries. we're always happy to help you out
                </div>
              </li>
            </ul>
          </Box>
        </Grid>
      </section>
      <section
        id="about"
        // className={classes.about}
        className="about"
      >
        <div
          // className={classes.aboutuptag}>
          className="aboutuptag"
        >
          {mobile
            ? "WHAT WE STAND BY ———————————————"
            : "WHAT WE STAND BY ————————————————————————————————————————"}
        </div>
        <div
          // className={classes.aboutuptagline}
          className="aboutuptagline"
        >
          THE FOUNDATION PRINCIPLES
        </div>
        <div
          // className={classes.abouthead}
          className="abouthead"
        >
          Be authentic. <br />
          Surrender outcome. <br />
          Do uncomfortable work.
        </div>
        <div
          // className={classes.aboutTag}
          className="aboutTag"
        >
          Not only are we built on great products, but also structured around
          some very basic and fundamental business philosophies about treating
          our culture shareholders and the community with respect and dignity.
          We call them our Foundation Principles
        </div>
      </section>
      <section
        id="products"
        // className={classes.product}
        className="product"
      >
        <div
          // className={classes.productuptag}
          className="productuptag"
        >
          {mobile
            ? "OUR PRODUCTS ———————————————"
            : "OUR PRODUCTS ——————————————————————————————————————————"}
        </div>
        <div
          // className={classes.workpeer}
          className="workpeer"
        >
          <img
            src={workpeerImg}
            alt="workpeerImg"
            style={{
              position: "absolute",
              left: "10%",
              width: "52%",
            }}
          />
        </div>
        <div
          // className={classes.producthead}
          className="producthead"
        >
          Workpeer
        </div>
        <div
          // className={classes.productsubhead}
          className="productsubhead"
        >
          A meeting scheduling platform that takes care of all your meetings, in
          one place.
        </div>
        <div
          // className={classes.productfeatures}
          className="productfeatures"
        >
          Effortless scheduling meets flawless video conferencing. Book meetings
          faster with the smart scheduling power of Workpeer.
        </div>
        <Link href="#">
          <a
            // className={classes.productlink}
            className="productlink"
          >
            Learn more about this project
          </a>
        </Link>
      </section>
      <section
        id="contact"
        // className={classes.contact}
        className="contact"
      >
        <div
          // className={classes.contactuptag}
          className="contactuptag"
        >
          {mobile
            ? "SAY HI TO THE TEAM! ———————————————"
            : "SAY HI TO THE TEAM! ——————————————————————————————————————————"}
        </div>
        <div
          // className={classes.contacthead}
          className="contacthead"
        >
          Let's talk
        </div>
        <div
          // className={classes.contactsubhead}
          className="contactsubhead"
        >
          Feel free to contact us and we'll get back to you as soon as we can.
        </div>
        <div
          // className={classes.info}
          className="info"
        >
          <div
          // className={classes.head}
          >
            ADDRESS
          </div>
          <div
          // className={classes.details}
          >
            Flat 201, Sanjana Enclave, Shanthi Nagar, Uppal Hyderabad - 500039
          </div>
          <div
          // className={classes.head}
          >
            SUPPORT
          </div>
          <div
          // className={classes.details}
          >
            contact@aztlan.in
          </div>
          <div
          // className={classes.head}
          >
            CONTACT
          </div>
          <div
          // className={classes.details}
          >
            +91 8977446032 <br />
            +91 9000725505
          </div>
        </div>
        <form
          // className={classes.form}
          className="form"
          noValidate
          autoComplete="off"
        >
          <TextField label="Name" />
          <TextField label="Email Address" />
          <TextField label="Tell us about it" multiline rows={5} />
          <Button
            className="button"
            type="submit"
            variant="contained"
            color="primary"
          >
            SUBMIT
          </Button>
        </form>
        {mobile && <img src={logoWhite} alt="aztlan-logo-white" className="logo-white"/>}
      </section>
    </div>
  );
};

export default HomeView;
