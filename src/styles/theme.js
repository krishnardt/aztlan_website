const themeMain = {
  palette: {
    primary: {
      main: "#D11621",
    },
    secondary: {
      main: "#000000",
    },
    text: {
      primary: "#181818",
      secondary: "#FFFFFF",
    },
  },
  typography: {
    fontFamily: "Montserrat",
  },
};

export default themeMain;