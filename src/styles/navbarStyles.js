import variables from "./variables";

const muiStyles = () => {
  return {
    section: {
      paddingLeft: "10%",
      paddingRight: "10%",
    },
    logo: {
      marginTop: "0",
      transform: "scale(0.75) translate(-27%,0%)",
    },

    gridposition: {
      transform: "translate(0%, -25%)",
    },
    list: {
      listStyle: "none",
      float: "right",
      "& li": {
        float: "left",
      },
      "& a": {
        textDecoration: "none",
        color: variables.textPrimary,
        fontWeight: "600",
        // fontSize: "110%",
        paddingRight: "30px",
        fontFamily: variables.font,
      },
    },
  };
};

export default muiStyles;
