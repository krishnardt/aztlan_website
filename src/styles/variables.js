const variables = {
  font: "Montserrat",
  primary: "#D11621",
  secondary: "#000000",
  textPrimary: "#646464",
  textSecondary: "#FFFFFF",
  fontset(min, max) {
    return `calc(${min}px + (${max} - 14) * ((100vw - 300px) / (1600 - 300)));`;
  },
};

export default variables;
