import variables from "./variables";
import { height } from "@material-ui/system";

const muiStyles = {
  section1: {
    height: "80vh",
    paddingLeft: "10%",
    paddingRight: "10%",
    marginTop: "5%",
  },
  homehead: {
    // fontSize: "5.5rem",
    fontSize: variables.fontset(20, 90),
    // fontFamily: variables.font,
    fontWeight: "700",
    lineHeight: "100%",
    // color: "#181818",
  },
  homeTagLine: {
    fontWeight: "400",
    // fontFamily: variables.font,
    lineHeight: "130%",
    fontFamily: variables.font,
    color: "#575757",
    marginBottom: "5%",
  },
  contactButton: {
    fontWeight: "600",
  },
  gridposition: {
    marginTop: "7%",
  },
  list: {
    listStyle: "none",
    padding: "0",
    "& li": {
      marginRight: "2%",
      float: "left",
      "& a": {
        textDecoration: "none",
        borderTop: "2px solid #B1B1B1",
        color: variables.textPrimary,
        fontFamily: variables.font,
        fontSize: "1.625rem",
        fontWeight: "600",
      },
      "& div": {
        display: "block",
        // width: "180px",
        width: "40%",
        fontFamily: variables.font,
        fontSize: "0.688rem",
        color: variables.textPrimary,
        fontWeight: "500",
        marginTop: "4%",
      },
    },
  },
  about: {
    height: "100vh",
    width: "100vw",
    margin: 0,
    padding: 0,
    backgroundColor: variables.primary,
    "& *": {
      marginLeft: "10%",
      fontFamily: variables.font,
      color: variables.textSecondary,
    },
  },
  aboutuptag: {
    paddingTop: "8%",
    fontSize: "0.788rem",
    fontWeight: "300",
    letterSpacing: "0.36px",
  },
  aboutuptagline: {
    fontWeight: "600",
    marginTop: "5%",
    letterSpacing: "1.15px",
  },
  abouthead: {
    marginTop: "1%",
    fontSize: variables.fontset(20, 75),
    // fontSize: "4.775rem",
    fontWeight: "500",
  },

  aboutTag: {
    marginTop: "6%",
    fontWeight: "300",
    fontSize: "0.998rem",
    width: "42.24vw",
  },

  product: {
    height: "100vh",
    margin: 0,
    padding: 0,
    "& *": {
      // marginLeft: "10%",
      fontFamily: variables.font,
      color: variables.textPrimary,
    },
  },
  productuptag: {
    marginLeft: "10%",
    paddingTop: "8%",
    fontSize: "0.788rem",
    fontWeight: "300",
    letterSpacing: "0.36px",
  },

  producthead: {
    // position: "absolute",
    position: "absolute",
    // left: "64.12vw",
    top: "13vh",
    left: "64vw",
    // top: "244vh",
    // fontSize: "3.75rem",
    // fontSize: "calc(14px + (70 - 14) * ((100vw - 300px) / (1600 - 300)))",
    fontSize: variables.fontset(14, 70),
    fontWeight: "700",
    color: variables.secondary,
  },
  productsubhead: {
    position: "absolute",
    // top: "258vh",
    top: "26vh",
    left: "64vw",
    width: "30vw",
    fontWeight: "700",
    fontSize: variables.fontset(10, 32),
    lineHeight: "112%",
  },
  productfeatures: {
    position: "absolute",
    left: "64vw",
    // top: "275vh",
    top: "43vh",
    width: "26vw",
    fontSize: variables.fontset(10, 22),
    fontWeight: "500",
    letterSpacing: "0.24px",
    lineHeight: "22px",
    color: "#434343",
  },
  productlink: {
    position: "absolute",
    left: "64vw",
    // top: "289vh",
    top: "57vh",
    color: variables.primary,
    textDecoration: "underline",
    fontSize: variables.fontset(10, 22),
    fontWeight: "700",
  },
  workpeer: {
    position: "relative",
    // height: "calc((768/1366)*90vw)",
    width: "90vw",
    maxWidth: "1366px",
    top: "10vh",
    minHeight: "192px",
    minWidth: "341.5px",
    // backgroundColor: "red",
  },
  contact: {
    position: "relative",
    height: "100vh",
    width: "100vw",
    margin: 0,
    padding: 0,
    backgroundColor: variables.secondary,
    "& div": {
      fontFamily: variables.font,
      color: variables.textSecondary,
    },
  },
  contactuptag: {
    paddingTop: "8%",
    fontSize: "0.788rem",
    fontWeight: "300",
    letterSpacing: "0.36px",
    marginLeft: "10%",
  },
  contacthead: {
    fontWeight: "700",
    fontSize: variables.fontset(10, 100),
    marginLeft: "10%",
    // marginTop: "0.2%",
  },
  contactsubhead: {
    marginTop: "3%",
    fontWeight: "300",
    fontSize: "0.998rem",
    width: "20.5vw",
    fontWeight: "300",
    marginLeft: "10%",
  },
  form: {
    display: "flex",
    flexDirection: "column",
    marginTop: "2%",
    "& > *": {
      marginLeft: "10%",
      marginTop: "2%",
      width: "30%",
    },
    "& .MuiInputBase-root": {
      borderBottom: "2px solid " + variables.textPrimary,
    },
    "& .Mui-focused": {
      borderBottom: "2px solid black",
    },
    "& .MuiButtonBase-root": {
      backgroundColor: variables.primary,
      color: variables.textSecondary,
      width: "10%",
    },
  },
  info: {
    position: "absolute",
    textAlign: "right",
    marginLeft: "60%",
    top: "54vh",
    right: "8%",
    width: "13.64vw",
  },
  head: {
    paddingBottom: "9px",
    fontWeight: "200",
  },
  details: {
    paddingBottom: "30px",
    fontWeight: "300",
  },
};

export default muiStyles;
