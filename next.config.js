// next.config.js
const withImages = require("next-images");
// module.exports = withImages();

const withCSS = require("@zeit/next-css");
module.exports = withCSS(withImages());

const withSass = require("@zeit/next-sass");
// module.exports = withSass({
//   cssModules: true,
// });
