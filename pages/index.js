import Topbar from "../src/components/Navbar";
import HomeView from "../src/components/HomeView";
// import variables from "../aztlan-frontend/src/styles/variables";
import { createMuiTheme, ThemeProvider } from "@material-ui/core";
import themeMain from "../src/styles/theme";

function Index() {
  const theme = createMuiTheme(themeMain);

  return (
    <ThemeProvider theme={theme}>
      <Topbar />
      <HomeView />
    </ThemeProvider>
  );
}

export default Index;
